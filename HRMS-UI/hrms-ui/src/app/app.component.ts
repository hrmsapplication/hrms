import { Component } from '@angular/core';
import { DataService } from './authentication/data.service';
import { AuthService } from './authentication/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
   constructor(private data:DataService, private auth:AuthService){
   }

   getAdmin() {
     this.data.getAuth('/admin').subscribe(d => {
       return console.log('getAdmin:' + d);
     });
   }

   getManager() {
    this.data.getAuth('/manager').subscribe(d => {
      return console.log('getManager:' + d);
    });
  }

  getAdminManager() {
    this.data.getAuth('/admin-manager').subscribe(d => {
      return console.log('getAdminManager:' + d);
    });
  }
  getAll() {
    this.data.getAuth('/all').subscribe(d => {
      return console.log('getAll:' + d);
    });
  }
   logout(){
     this.auth.logout();
   }
}
