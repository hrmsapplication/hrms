import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import {AuthService} from '../authentication/auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TokenInterceptor} from '../authentication/token-interceptor';
import {DataService} from '../authentication/data.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,BrowserModule,HttpClientModule,
  ],
  providers: [
    AuthService,
    DataService,
    {provide: APP_INITIALIZER, useFactory: kcFactory, deps:[AuthService], multi:true},
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AuthenticationModule { }
export function kcFactory(keycloakService: AuthService){
  return () => keycloakService.init();
}
