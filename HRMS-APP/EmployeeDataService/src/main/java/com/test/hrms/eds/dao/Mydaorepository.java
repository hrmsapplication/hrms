package com.test.hrms.eds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
import com.test.hrms.eds.entity.Employee;
 
@Repository
public interface Mydaorepository extends JpaRepository<Employee, Integer> {
 
}