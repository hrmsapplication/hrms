package com.test.hrms.eds.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
@Getter
@Setter
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "EMPLOYEE_ID")
  private Integer employeeId;

  @Column(name = "EMPLOYEE_NUMBER")
  private Integer employeeNumber;

  @Column(name = "EMPLOYEE_NAME")
  private String employeeName;

  @Column(name = "EMPLOYEE_INDENTITY")
  private String employeeIdentity;

  @Column(name = "EMPLOYEE_ADDRESS")
  private String employeeAddress;

  @Column(name = "EMPLOYEE_CONTACT_NUMBER")
  private Long employeeContactNumber;

  @Column(name = "EMPLOYEE_BLOOD_GROUP")
  private Long employeeBloodGroup;
  
  @Override
	public String toString() {
		return "Employee [id=" + employeeId + ", name=" + employeeName + ", employeeNumber=" + employeeNumber + ", employeeAddress=" + employeeAddress + "]";
	}

}
