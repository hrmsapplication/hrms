package com.test.hrms.eds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class EdsApplication {

  @GetMapping("/welcome")
  public void welcome() {
    System.out.println("Welcome");
  }

  public static void main(String[] args) {
    SpringApplication.run(EdsApplication.class, args);
  }

}

